export const environment = {
    projectId: '',
    keyFile: '',
    firebaseWebApiKey: '',
    dynamicApiUrl: 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks',
    dynamicShortUrl: '',
    watchFolderPath: '',
    bucketName: '',
    urlPrefix: ''
};
