import * as fs from 'fs';
import * as id from 'shortid';
import * as mimeCheck from 'mime/lite';
import * as fetch from 'node-fetch';
import * as notification from 'node-notifier';
import {Storage} from '@google-cloud/storage';
import {environment} from "./environments/environment";


const gcp = new Storage({
    projectId: environment.projectId,
    keyFilename: environment.keyFile,
});

class Main {

    watchFolder: string = environment.watchFolderPath;
    private bucketName = environment.bucketName;
    private deleteAfterSave: true;

    constructor() {
        fs.watch(this.watchFolder, async (eventType, filename) => {
            console.log(eventType, filename);
            if (filename && filename.split('.')[0] !== '') {
                const url = await this._uploadToGcp(filename);
                if (url !== '') {
                    const shortenUrl = await this._shortenUrl(url);
                    this._addUrlToClipBoard(shortenUrl);
                    this._sendNotification(filename, shortenUrl);
                    setTimeout(() => {
                        this._removeUploadedFile(filename);
                    }, 3000)
                }
            }
        });
    }

    private _uploadToGcp(fileName: string): Promise<string> {
        const fileNameSplit = fileName.split('.');
        const fileNameExt = fileNameSplit[fileNameSplit.length - 1];

        const fullFileName = `${this.watchFolder}/${fileName}`;
        const uploadFileName = `${environment.urlPrefix}-${id.generate()}.${fileNameExt}`;

        if (!fs.existsSync(fullFileName)) {
            return Promise.resolve('')
        }

        return gcp.bucket(this.bucketName).upload(fullFileName, {
            gzip: true,
            metadata: {
                cacheControl: 'public, max-age=31536000',
                contentType: mimeCheck.getType(fileName),
            },
            public: true,
            destination: this._buildUploadPath(uploadFileName),
        })
            .then(res => `https://${this.bucketName}/${res[0].name}`)
            .catch(err => {
                console.error(err);
                return '';
            });
    }

    private _buildUploadPath(fileName: string): string {
        const date = new Date();
        const month = (date.getMonth() + 1) < 10 ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;

        return `upload/${date.getFullYear()}/${month}/${fileName}`;
    }

    private _shortenUrl(url: string): Promise<string> {
        const body = {
            dynamicLinkInfo: {
                domainUriPrefix: environment.dynamicShortUrl,
                link: url,
            }
        };

        // @ts-ignore
        return fetch(`${environment.dynamicApiUrl}?key=${environment.firebaseWebApiKey}`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {'Content-Type': 'application/json'},
        })
            .then(res => res.json())
            .then((json) => json.shortLink)
            .catch(err => {
                console.error(err);
                return '';
            })
    }

    private _addUrlToClipBoard(url: string) {
        const proc = require('child_process').spawn('pbcopy');
        proc.stdin.write(url);
        proc.stdin.end();
    }

    private _sendNotification(fileName: string, link: string): void {
        notification.notify({
                title: 'Added link to clipboard!',
                message: link,
                icon: `${this.watchFolder}/${fileName}`, // Absolute path (doesn't work on balloons)
                sound: true, // Only Notification Center or Windows Toasters
                wait: true // Wait with callback, until user action is taken against notification
            },
            (err, response) => {
                console.error(err);
                // Response is response from notification
            }
        );
    }

    private _removeUploadedFile(fileName: string): void {
        fs.unlinkSync(`${this.watchFolder}/${fileName}`);
    }
}

new Main();
