# Node File Uploader
> This project was built so I could have the functionality of taking a screenshots, upload it to a gcp bucket, shorten the returned URL, then add it to the clipboard.

![License | MIT](https://badgen.net/badge/license/MIT/blue) ![Gluten | Free](https://badgen.net/badge/Gluten/Free/green)

### Todo
* [ ] Run with [Launchd](https://stackoverflow.com/questions/4018154/how-do-i-run-a-node-js-app-as-a-background-service/25998406#25998406)
* [ ] Add some tests

### How to use

1. Rename `environment.example.ts` to `environment.ts`
2. Update `environment.ts` with
    - `projectId` with the id of your GCP project
    - `keyFile` with the path to the [service account json file](https://console.cloud.google.com/iam-admin/serviceaccounts)
    - `firebaseWebApiKey`: Web Api from the your firebase project
    - `dynamicApiurl`: Already filled out, api endpoint for generating short urls
    - `dynamicShortUrl`: url from firebase dynamic links, i.e. `https://example/page.link`
    - `watchFolderPath`: path to the folder to watch for new files
    - `bucketName`: name of GCP bucket
    - `urlPrefix`: string to place in front of file random id, i.e. `[something]-7o5ZR99MS.ext`
3. When taking a screenshot make sure it's saved in the folder you have as your `watchFolderPath` as the place it's saving to.
4. When saving the file will be added uploaded to your bucket, the url shortened, then the url added to your clipboard. 
    - A notification will show when the url is added to your clipboard
